const testFolder = '../cache';
const fs = require('fs');

var projectCache;

module.exports = function (controller) {
    fs.readdirSync(testFolder).forEach(function (file) {
        //console.log("haha = " + file);
        //console.log(testFolder);
        if(file === 'project'){
            projectCache = require(testFolder+'/'+file+'/projectCache')(controller);
        }
    });
    return{
        projectCache : function () {
            return projectCache;
        }
    }
}