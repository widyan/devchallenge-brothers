
var Sequelize = require('sequelize');
var db = require('./connection');
var schemaTable = require('./TableSchema');


db.get().sync({force:true}).then(function () {
    if(schemaTable().getTbUser()){
        console.log("tb_user created");
    }

    if(schemaTable().getTbProject()){
        console.log("tb_project created");
    }
}).catch(function (err) {
    console.log("table not created, something wrong!!, " + err.message);
}).done();
