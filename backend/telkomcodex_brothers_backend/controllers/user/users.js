var Utils = require('../../utils/utils');
var async = require('async');

module.exports = function (models) {
    return {
        login : function (req, res) {
            if(!req.body.email || !req.body.password){
                return res.status(400).send(
                    {
                        status: false,
                        message: 'You must send the complite parameter',
                        result: null
                    });
            }else{
                var data = {
                    email   : req.body.email,
                    password : req.body.password
                };

                console.log("Data = " + JSON.stringify(data));
                models.getUserModel().auth_login(data, function (status, message, result) {
                    return res.status(200).send({
                        status: status,
                        message: message,
                        result: result
                    })
                });
            }
        }
    }
}