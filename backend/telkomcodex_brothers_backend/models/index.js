var db = require('../config/connection');
var tableSchemas = require('../config/TableSchema');

const testFolder = '../models';

//require('../controller/tes/quranindonesia');
const fs = require('fs');

//var quranindonesia = require('../controller/tes/quranindonesia');
var project;
var user;

module.exports = function (db, tableSchemas) {
    fs.readdirSync(testFolder).forEach(function (file) {
        //console.log("haha = " + file);
        //console.log(testFolder);
        /*if(file === 'project'){
            project = require(testFolder+'/'+file+'/projects.model')(db, tableSchemas);
        }*/
        if(file === 'user'){
            user = require(testFolder+'/'+file+'/users.model')(db, tableSchemas);
        }
        //quranarab = require(testFolder+'/quranarab')(models);
        //quransurah = require(testFolder+'/quransurah')(models);
    });
    return{
        getProjectModel : function () {
            return project;
        },
        getUserModel : function () {
            return user;
        }
    }
}