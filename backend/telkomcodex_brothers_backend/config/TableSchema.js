var Sequelize = require('sequelize');
var db = require('./connection');

var tb_user = db.get().define('tb_user',{
    username    : Sequelize.STRING(25),
    email       : Sequelize.STRING(25),
    pass        : Sequelize.STRING(255),
    status      : Sequelize.INTEGER,
    auth        : Sequelize.STRING(255),
    role        : Sequelize.INTEGER,
    status      : Sequelize.INTEGER
});

var tb_project = db.get().define('tb_project',{
    project_name    : Sequelize.STRING(50),
    project_unit    : Sequelize.STRING(10),
    project_stakeholder : Sequelize.STRING(50),
    project_leader  : Sequelize.STRING(50),
    project_sprint  : Sequelize.INTEGER,
    status          : Sequelize.INTEGER,
    comment         : Sequelize.STRING(255)
});

module.exports = function () {
    return{
        getTbUser : function () {
            return tb_user;
        },
        getTbProject : function () {
            return tb_project;
        }
    }
}

