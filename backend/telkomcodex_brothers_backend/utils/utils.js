var querystring = require('querystring');
var http = require('http');
var axios = require('axios');

var host = 'jira-telkomdds-devops-playground.apps.playcourt.id';
var username = 'devchallenge';
var password = 'dev12345';
var sessionId = null;
var moment = require('moment');

var date = new Date();

exports.requestData = function(endpoint, method, data, success) {
    //console.log("TES = " + new Buffer(username + ':' + password).toString('base64'));
    console.log("Data = " + JSON.stringify(data));
    var dataString = JSON.stringify(data);
    var headers = {
        'Authorization'     : 'Basic ' + new Buffer(username + ':' + password).toString('base64')
    };

    if(method == 'GET'){
        endpoint += '?' + querystring.stringify(data);
        //console.log("URL = " + host + endpoint);
        //console.log("data = " + data);
        //console.log("querystring data = " + querystring.stringify(data));
    }

    var options = {
        host    : host,
        path    : endpoint,
        method  : method,
        headers : headers
    };

    var req = http.request(options, function(res) {
        //res.setEncoding('utf-8');

        var responseString = '';

        res.on('data', function(data) {
            //console.log("DATA = " + data)
            responseString += data;
        });

        res.on('end', function() {
            //console.log("asdf = " + responseString);
            var responseObject = JSON.parse(responseString);
            //return responseObject;
            success(responseObject);
        });
    });

    req.write(dataString);
    req.end();

}

exports.requestData2 = async(endpoint, method, data)=>{
    //console.log("TES = " + new Buffer(username + ':' + password).toString('base64'));
    var dataString = JSON.stringify(data);
    var headers = {
        'Authorization'     : 'Basic ' + new Buffer(username + ':' + password).toString('base64')
    };

    if(method == 'GET'){
        endpoint += '?' + querystring.stringify(data);
        //console.log("URL = " + host + endpoint);
        //console.log("data = " + data);
        //console.log("querystring data = " + querystring.stringify(data));
    }

    var options = {
        url    : 'http://jira-telkomdds-devops-playground.apps.playcourt.id'+endpoint,
        method  : method,
        headers : headers,
        responseType:'application/json'
    };

    try{
        const response = await axios(options);
        return response.data;
    }catch (err){
        console.log("Err = " +err.message);
    }

}

exports.find_duplicate_in_array = function(arra1) {
    var object = {};
    var result = [];

    arra1.forEach(function (item) {
        console.log(item);
        if(!object[item])
            object[item] = 0;
        object[item] += 1;
    })

    console.log(JSON.stringify(object));
    for (var prop in object) {
        console.log(JSON.stringify(object[prop]));
        if(object[prop] >= 2) {
            console.log("IYA = " + object[prop]);
            result.push(prop);
        }
    }

    return result;

}

exports.parseDate = function (str) {
    //var str = '2011-04-11T10:20:30Z';
    var date = moment(str);
    var dateComponent = date.utc().format('YYYY-MM-DD');
    var timeComponent = date.utc().format('HH:mm:ss');

    return dateComponent;
    //console.log(dateComponent);
    //console.log(timeComponent);
}

exports.differentDays = function (datestr1, datestr2) {
    /*var date1 = new Date("7/11/2010");
    var date2 = new Date("12/12/2010");*/
    var date1 = new Date(datestr1);
    var date2 = new Date(datestr2);
    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

    return diffDays;
    //alert(diffDays);
}

exports.getCurrentDateTime = function () {
    return date;
}

exports.sortByProperty = function (property) {

    return function (x, y) {

        return ((x[property] === y[property]) ? 0 : ((x[property] < y[property]) ? 1 : -1));

    };

};

exports.insertItemObjectToArray = function (objStr) {
    /*var objectRole = {
        "UX": "http://jira-telkomdds-devops-playground.apps.playcourt.id/rest/api/2/project/10001/role/10211",
        "Mobile Frontend": "http://jira-telkomdds-devops-playground.apps.playcourt.id/rest/api/2/project/10001/role/10202",
        "SA": "http://jira-telkomdds-devops-playground.apps.playcourt.id/rest/api/2/project/10001/role/10203",
        "QA": "http://jira-telkomdds-devops-playground.apps.playcourt.id/rest/api/2/project/10001/role/10207",
        "Backend": "http://jira-telkomdds-devops-playground.apps.playcourt.id/rest/api/2/project/10001/role/10200",
        "DevOps": "http://jira-telkomdds-devops-playground.apps.playcourt.id/rest/api/2/project/10001/role/10206",
        "UI": "http://jira-telkomdds-devops-playground.apps.playcourt.id/rest/api/2/project/10001/role/10210",
        "SH": "http://jira-telkomdds-devops-playground.apps.playcourt.id/rest/api/2/project/10001/role/10205",
        "Web Frontend": "http://jira-telkomdds-devops-playground.apps.playcourt.id/rest/api/2/project/10001/role/10201",
        "Administrators": "http://jira-telkomdds-devops-playground.apps.playcourt.id/rest/api/2/project/10001/role/10002",
        "Doc": "http://jira-telkomdds-devops-playground.apps.playcourt.id/rest/api/2/project/10001/role/10208",
        "SM": "http://jira-telkomdds-devops-playground.apps.playcourt.id/rest/api/2/project/10001/role/10204",
        "PO": "http://jira-telkomdds-devops-playground.apps.playcourt.id/rest/api/2/project/10001/role/10209"
    };*/
    var newArray = [];

    for(object in objStr){
        newArray.push({
            roles   : object,
            self    : objStr[object]
        });
    }

    return newArray;

}