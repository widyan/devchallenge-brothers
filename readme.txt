Brothers :

Muhammad Widyan Riadhi Fakhrun
Hilman Taris Muttaqin

untuk menjalankan aplikasinya, harus menjalankan redis terlebih dahulu. karena dalam projek aplikasinya menggunakan redis untuk caching
untuk backend terdapat dokumentasi API yang terdapat dalam folder 'docs_api'
database untuk data user menggunakan postgresql, dengan nama database telkomcodex.
untuk menjalankan backend masuk terlebih dahulu kedalam folder 'bin' lalu jalankan di console dengan perintah 'node www'
untuk menjalankan frontend menggunakan perintah 'npm start'

setelah itu keduanya akan terintegrasi.
kebanyakan dalam aplikasi ini adalah mengolah data dari API Jira, untuk database saat ini hanya baru digunakan untuk login saja.