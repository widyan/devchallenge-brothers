var db = require('../config/connection');
var tableSchemas = require('../config/TableSchema');
var models = require('../models/index')(db, tableSchemas());

const testFolder = '../controllers';

//require('../controller/tes/quranindonesia');
const fs = require('fs');

//var quranindonesia = require('../controller/tes/quranindonesia');
var project;
var team;
var user;

module.exports = function (models) {
    fs.readdirSync(testFolder).forEach(function (file) {
        //console.log("haha = " + file);
        //console.log(testFolder);
        if(file === 'project'){
            project = require(testFolder+'/'+file+'/projects')(models);
        }
        if(file === 'team'){
            team = require(testFolder+'/'+file+'/teams')(models);
        }
        if(file === 'user'){
            user = require(testFolder+'/'+file+'/users')(models);
        }
        //quranarab = require(testFolder+'/quranarab')(models);
        //quransurah = require(testFolder+'/quransurah')(models);
    });
    return{
        projectController : function () {
            return project;
        },
        teamController : function () {
            return team;
        },
        userController : function () {
            return user;
        }
    }
}
