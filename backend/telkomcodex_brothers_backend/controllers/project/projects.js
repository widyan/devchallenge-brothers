var Utils = require('../../utils/utils');
var async = require('async');
var model = require('../../models/project/projects.model');
const redis = require('redis');
const client = redis.createClient();

module.exports = function (models) {
    return {
        getListProject: function (req, res) {
            async.waterfall([
                function listBoard(cb) {
                    Utils.requestData('/rest/agile/1.0/board', 'GET', null, function (listBoard) {
                        return cb(null, listBoard);
                    });
                },
                function listProjectAndSprint(board, cb) {
                    let arrboard = board.values;

                    getProject(function (newBoard) {
                        getListSprint(newBoard, function (boardResult) {
                            return cb(null, boardResult)
                        })
                    })

                    function getProject(callback) {
                        for (let k = 0; k < arrboard.length; k++) {
                            Utils.requestData('/rest/agile/1.0/board/' + arrboard[k].id + '/project', 'GET', null, function (project) {
                                arrboard[k].project = project.values;
                                if (k == (arrboard.length - 1)) {
                                    callback(arrboard);
                                }
                            });
                        }
                    }

                    function getListSprint(boardProject, callback) {
                        for (let i = 0; i < boardProject.length; i++) {
                            Utils.requestData('/rest/agile/1.0/board/' + boardProject[i].id + '/sprint', 'GET', null, function (sprints) {
                                boardProject[i].total_sprint = sprints.values.length;
                                if (i == (boardProject.length - 1)) {
                                    callback(boardProject);
                                }
                            });
                        }
                    }
                }
            ], function (err, result) {
                //console.log("PERSIB = " + JSON.stringify(result))
                return res.status(200).send({
                    status: 'success',
                    message: 'success get list project',
                    result: result
                });
            })
        },

        getDetailProject: function (req, res) {
            //Utils.parseDate();
            if (!req.query.id_board || !req.query.key_project) {
                return req.status(200).send({
                    status: 'error',
                    message: 'You must send complete parameter',
                    result: null
                })
            } else {
                var key_project = req.query.key_project;
                let id_board = req.query.id_board;
                console.log("KEY PROJECT = " + key_project);
                console.log("ID BOARD = " + id_board);
                async.waterfall([
                    function detailProject(cb) {
                        Utils.requestData('/rest/api/2/project/' + key_project, 'GET', null, function (detailProject) {
                            return cb(null, detailProject);
                        });
                    },
                    function listRole(project, cb) {
                        Utils.requestData('/rest/api/2/project/' + key_project + '/role', 'GET', null, function (listRole) {
                            var arrRoles = Utils.insertItemObjectToArray(listRole);

                            getRoleAndUser(function (roleAndUser) {
                                project.member = roleAndUser;
                                return cb(null, project);
                            })

                            function getRoleAndUser(callback) {
                                let userAndRole = [];
                                for (let i = 0; i < arrRoles.length; i++) {
                                    Utils.requestData(arrRoles[i].self, 'GET', null, function (detailRole) {
                                        var actors = detailRole.actors;
                                        if (actors.length != 0) {
                                            for (var j = 0; j < actors.length; j++) {
                                                actors[j].role = detailRole.name;
                                                userAndRole.push(actors[j])
                                            }
                                        }
                                        if (i == (arrRoles.length - 1)) {
                                            callback(userAndRole);
                                        }
                                        //return cb(null, detailProject);
                                    });
                                }
                            }
                        });
                    },
                    /*function listMember(project, cb) {
                        Utils.requestData('/rest/api/2/user/assignable/search','GET', {project:key_project}, function (memberProject) {
                            project.member = memberProject;
                            return cb(null, project);
                        });
                    },*/
                    function getSprint(project, cb) {
                        //console.log()
                        //Utils.parseDate()

                        getListSprint(function (newProject) {
                            return cb(null, newProject);
                        })

                        function getListSprint(callback) {
                            Utils.requestData('/rest/agile/1.0/board/' + id_board + '/sprint', 'GET', null, function (sprints) {
                                var sprint = sprints.values;
                                project.start_date = sprint[0].startDate;
                                project.end_date = sprint[(sprint.length) - 1].endDate;
                                project.range = (Utils.differentDays(Utils.parseDate(sprint[0].startDate), Utils.parseDate(sprint[(sprint.length) - 1].endDate)));
                                project.tersisa = (Utils.differentDays(Utils.parseDate(Utils.getCurrentDateTime()), Utils.parseDate(sprint[(sprint.length) - 1].endDate)));
                                for (var i = 0; i < sprint.length; i++) {
                                    if (sprint[i].state === 'active') {
                                        project.sprint_active = sprint[i];
                                    }
                                }
                                callback(project);
                            });
                        }
                    }
                ], function (err, result) {
                    //console.log("PERSIB = " + JSON.stringify(result))
                    return res.status(200).send({
                        status: 'success',
                        message: 'success get detail project',
                        result: result
                    });
                })
            }
        },
        getStatisticAllJiraProject: async (req, res) =>{
            async.waterfall([
                function listProject(cb) {
                    Utils.requestData('/rest/api/2/project', 'GET', null, function (listProject) {
                        return cb(null, listProject);
                    });
                },
                async function totalIssue(listProject) {
                    let statistics = {};

                    statistics.total_project = listProject.length;

                    const totalIssue = await model.getTotalIssue(listProject);
                    const totalIssueProgress = await model.getTotalIssueProgress(listProject);
                    const totalIssueDone = await model.getTotalIssueDone(listProject);
                    const totalIssueToDo = await model.getTotalIssueToDo(listProject);
                    const totalIssueBug = await model.getTotalIssueBug(listProject);

                    statistics.total_issue = totalIssue.totalIssue;
                    statistics.total_issueProgress = totalIssueProgress.totalIssueProgress;
                    statistics.total_issueDone = totalIssueDone.totalIssueDone;
                    statistics.total_issueTodo = totalIssueToDo.totalIssueToDo;
                    statistics.total_issueBug = totalIssueBug.totalIssueBug;

                    return statistics;
                }
            ], function (err, result) {

                var object = {
                    status: 'success',
                    message: 'success get statistik',
                    result: result
                };
                client.setex('detail',5000, JSON.stringify(object));
                //console.log("STATISTIKS = " + JSON.stringify(result));
                return res.status(200).send(object);
            })
        },
        getStatisticProject: function (req, res) {
            
        },
        getBestProductPerformace : function (req, res) {
            async.waterfall([
                function getListProject(cb) {
                    Utils.requestData('/rest/api/2/project', 'GET', null, function (listProject) {
                        return cb(null, listProject);
                    });
                },
                /*
                function getBoard(listProject, cb) {
                    Utils.requestData('/rest/agile/1.0/board', 'GET', null, function (listBoard) {
                        return cb(null, listBoard, listProject);
                    });
                },*/
                async function bestPerformance(listProject) {

                    let statistics = {};
                    try{
                        const totalIssueDone = await model.getTotalIssueDoneByProject(listProject);
                        //const sprintActive = await model.getSprintActiveInProject(listBoard, totalIssueDone);
                        statistics = totalIssueDone;

                    }catch (err){
                        console.log("HHHH = " + err);
                    }

                    return statistics;
                }
            ],function (err, result) {

                //console.log(result);
                var sortedData = result.sort(Utils.sortByProperty('totalIssueDone'));
                var object = {
                    status: 'success',
                    message: 'success get product performance',
                    result: sortedData
                };
                return res.status(200).send(object);
            })
        },
        getBestTeamPerformance : function (req, res) {
            async.waterfall([
                function getListProject(cb) {
                    Utils.requestData('/rest/api/2/project', 'GET', null, function (listProject) {
                        return cb(null, listProject);
                    });
                },
                async function bestPerformance(listProject) {

                    const totalIssueToDoToday = await model.getIssueToDoTodayByProject(listProject);
                    const totalIssueProgressToday = await model.getIssueInProgressTodayByProject(totalIssueToDoToday);
                    const totalIssueDoneToday = await model.getIssueDoneTodayByProject(totalIssueProgressToday);

                    return totalIssueDoneToday;
                }
            ],function (err, result) {
                var sortedData = result.sort(Utils.sortByProperty('totalIssueDoneToday','totalIssueProgressToday','totalIssueToDoToday'));
                var object = {
                    status: 'success',
                    message: 'success get best team performance',
                    result: sortedData
                };
                return res.status(200).send(object);
            });
        },
        getDetailPointMemberInProject : async (req, res) =>{
            const a = await model.getPointMemberByProject(null, 4);
            console.log("TAH = " + JSON.stringify(a));
            //console.log(Utils.find_duplicate_in_array(["tes","tes","haha","klkl"]));
        },
        getIssueDoneProject : function (req, res) {

        },
        getIssueTodoProject : function (req, res) {

        },
        getIssueInProgressProject : function (req, res) {

        },
        getIssueBugProject : function (req, res) {

        }
    }
}