var Utils = require('../../utils/utils');
var Sequelize = require('sequelize');
var async = require('async');
var bcrypt = require('bcrypt-nodejs');

module.exports = function (db, tableSchema) {
    return {
        auth_login : async (data, done) =>{
            try{
                /*var password = bcrypt.hashSync('testing123', null, null);
                const data_user = await tableSchema.getTbUser().create({
                    email : 'hilmantm@gmail.com',
                    username : 'hilman',
                    pass : password,
                    role : 1,
                    status:1
                });

                console.log("berhasil = " + JSON.stringify(data_user));*/
                const data_user = await tableSchema.getTbUser().find({
                    where : {
                        email : data.email
                    }
                });
                //console.log("TES = " + JSON.stringify(data_user.pass));
                if(data_user != null){
                    if(!bcrypt.compareSync(data.password,data_user.pass)){
                        console.log("Kata Sandi Salah");
                        return done(false, "Kata Sandi Salah", null);
                    }else{
                        console.log("Kata Sandi Benar");
                        return done(true, "Kata Sandi Benar", data_user);
                    }
                }else{
                    return done(false, "Email Tidak Terdaftar", null);
                }
            }catch (err){
                console.log("Err = " + err);
            }
        }
    }
}