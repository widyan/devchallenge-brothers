var Utils = require('../../utils/utils');
var async = require('async');

exports.getTotalIssue = async(listProject)=>{
    var statistics = {};
    let totalIssue = 0;
    for (let i = 0; i < listProject.length; i++) {
        const aduh = await Utils.requestData2('/rest/api/2/search', 'GET', {jql:'project='+listProject[i].key,fields:'total'});
        totalIssue = totalIssue + aduh.total;
    }
    statistics.totalIssue = totalIssue;
    return statistics;
}

exports.getTotalIssueProgress = async(listProject)=> {
    var statistics = {};
    var totalIssueProgress = 0;
    for (let j = 0; j < listProject.length; j++) {
        const aduh = await Utils.requestData2('/rest/api/2/search', 'GET', {jql:"project="+listProject[j].key+" and status='In Progress'",fields:'id, key, status'});
        totalIssueProgress = totalIssueProgress + aduh.total;
    }
    statistics.totalIssueProgress = totalIssueProgress;
    return statistics;
}

exports.getTotalIssueDone = async(listProject) =>{
    var statistics = {};
    var totalIssueDone = 0;
    for (let k = 0; k < listProject.length; k++) {
        const aduh = await Utils.requestData2('/rest/api/2/search', 'GET', {jql:"project="+listProject[k].key+" and status='Done'",fields:'id, key, status'});
        totalIssueDone = totalIssueDone + aduh.total;
    }
    statistics.totalIssueDone = totalIssueDone;
    return statistics;
}

exports.getTotalIssueToDo = async(listProject) =>{
    var statistics = {};
    var totalIssueToDo = 0;
    for (let l = 0; l < listProject.length; l++) {
        const aduh = await Utils.requestData2('/rest/api/2/search', 'GET', {jql:"project="+listProject[l].key+" and status='To Do'",fields:'id, key, status'});
        totalIssueToDo = totalIssueToDo + aduh.total;
    }
    statistics.totalIssueToDo = totalIssueToDo;
    return statistics;
}

exports.getTotalIssueBug = async(listProject) =>{
    var statistics = {};
    var totalIssueBug = 0;
    for (let m = 0; m < listProject.length; m++) {
        const aduh = await Utils.requestData2('/rest/api/2/search', 'GET', {jql:"project="+listProject[m].key+" and issueType='Bug'",fields:'id, key, issuetype'});
        totalIssueBug = totalIssueBug + aduh.total;
    }
    statistics.totalIssueBug = totalIssueBug;
    return statistics;
}

exports.getTotalIssueStory=async(listProject) =>{
    var statistics = {};
    var totalIssueStory = 0;
    for (let n = 0; n < listProject.length; n++) {
        const aduh = await Utils.requestData2('/rest/api/2/search', 'GET', {jql:"project="+listProject[n].key+" and issueType='Story'",fields:'id, key, issuetype'});
        totalIssueStory = totalIssueStory + aduh.total;
    }
    statistics.totalIssueBug = totalIssueStory;
    return statistics
}

exports.getSprintDoneThisWeek = async(listProject)=>{

}

exports.getIssueToDoTodayByProject = async(listProject)=>{
    //let statistics = {};
    var count = 0;
    var currentDate = Utils.parseDate(Utils.getCurrentDateTime());
    for (var k = 0; k < listProject.length; k++) {
        const aduh = await Utils.requestData2('/rest/api/2/search', 'GET', {jql:"project="+listProject[k].key+" and status='To Do'",fields:'updated'});
        var issues = aduh.issues;
        for(var i = 0;i<issues.length;i++){
            console.log("Key = " + listProject[k].key + ", total issue todo = " + issues.length);
            if(currentDate === Utils.parseDate(issues[i].fields.updated)){
                console.log(issues[i].fields.updated);
                console.log(currentDate + " = " + Utils.parseDate(issues[i].fields.updated));
                count++;
                //console.log("Sama ke = " + count);
            }
            listProject[k].totalIssueToDoToday = count;
        }
        count = 0;
    }
    return listProject;
}

exports.getIssueInProgressTodayByProject = async(listProject)=>{
    //let statistics = {};
    var count = 0;
    for (var k = 0; k < listProject.length; k++) {
        const aduh = await Utils.requestData2('/rest/api/2/search', 'GET', {jql:"project="+listProject[k].key+" and status='In Progress'",fields:'updated'});
        var currentDate = Utils.parseDate(Utils.getCurrentDateTime());
        var issues = aduh.issues;
        for(var i = 0;i<issues.length;i++){
            if(currentDate === Utils.parseDate(issues[i].fields.updated)){
                count++;
            }
            listProject[k].totalIssueProgressToday = count;
        }
        count = 0;
    }
    return listProject;
}

exports.getIssueDoneTodayByProject = async(listProject)=>{
    //let statistics = {};
    var count = 0;
    for (var k = 0; k < listProject.length; k++) {
        const aduh = await Utils.requestData2('/rest/api/2/search', 'GET', {jql:"project="+listProject[k].key+" and status='Done'",fields:'updated'});
        var currentDate = Utils.parseDate(Utils.getCurrentDateTime());
        var issues = aduh.issues;
        for(var i = 0;i<issues.length;i++){
            if(currentDate === Utils.parseDate(issues[i].fields.updated)){
                count++;
            }
            listProject[k].totalIssueDoneToday = count;
        }
        count = 0;
    }
    return listProject;
}

exports.getTotalIssueDoneByProject = async(listProject)=>{
    //let statistics = {};
    for (var k = 0; k < listProject.length; k++) {
        const aduh = await Utils.requestData2('/rest/api/2/search', 'GET', {jql:"project="+listProject[k].key+" and status='Done'",fields:'id, key, status'});
        //statistics = {totalIssueDone : aduh.total};
        //console.log(aduh.total)
        listProject[k].totalIssueDone = aduh.total;
    }
    return listProject;
}

exports.getSprintActiveInProject = async(listBoard, listProject)=>{
    let statistics = {};
    var boardValues = listBoard.values;
    //for(let j = 0;j<listProject.length;j++){
        for(var k = 0; k < boardValues.length;k++){
            const aduh = await Utils.requestData2('/rest/agile/1.0/board/'+boardValues[k].id+'/sprint', 'GET', null);
            var sprint = aduh.values;
            for (var i = 0; i < sprint.length; i++) {
                if (sprint[i].state === 'active') {
                    var idSprint = sprint[i].id;
                    const backloginsprint = await Utils.requestData2('/rest/agile/1.0/board/'+boardValues[k].id+'/sprint/'+idSprint+"/issue", 'GET', null);
                    console.log("i = "+(i+1)+" , active sprint = " + JSON.stringify(sprint[i])+" , totalSprint = " + backloginsprint.total);
                    listProject[k].totalSprint = sprint.length;
                    listProject[k].sprint_active = sprint[i].name;
                    listProject[k].totalBacklogInSprint = backloginsprint.total;
                }
            }
        }
    //}
    return listProject;
}

exports.getTotalBacklogInSprintProject = async(listBoard, listProject)=>{
    let statistics = {};
    //listProject.statistics = {};
    var boardValues = listBoard.values;
    for(let i = 0;i<listProject.length;i++){
        for (let k = 0; k < boardValues.length; k++) {
            const aduh = await Utils.requestData2('/rest/agile/1.0/board/'+boardValues[k].id+'/', 'GET', {field:"total"});
            statistics.totalBacklogInSprint = aduh.total;
            listProject[i].statistics = statistics;
        }
    }
    return listProject;
}

exports.getPointMemberByProject = async(codeProject, idBoard)=>{
    var statistic = {};
    var pointQueque = [];
    var pointRemaining = [];
    var pointBurn = [];
    const backlog = await Utils.requestData2('/rest/agile/1.0/board/'+idBoard+'/backlog', 'GET', {fields:"assignee, customfield_10106"});
    //const done = await Utils.requestData2('/rest/api/2/search', 'GET', {jql:"project="+codeProject+" and status='Done'",fields:'assignee, customfield_10106'});
    //const todos = await Utils.requestData2('/rest/api/2/search', 'GET', {jql:"project="+codeProject+" and status='To Do'",fields:'assignee, customfield_10106'});
    //const progress = await Utils.requestData2('/rest/api/2/search', 'GET', {jql:"project="+codeProject+" and status='In Progress'",fields:'assignee, customfield_10106'});

    var backlogIssues = backlog.issues;
    var queque = 0;
    //console.log(backlogIssues.length);
    var tes = backlogIssues.filter((set => f => !set.has(f.fields.assignee.key) && set.add(f.fields.assignee.key))(new Set));
    console.log(JSON.stringify(tes));
    /*for(var i = 0;i<backlogIssues.length-1;i++){
        //console.log("jjjj = " + JSON.stringify(backlogIssues[i].fields.assignee.key));
        if(backlogIssues.length > 2){
            if(backlogIssues[i+1].fields.assignee.key === backlogIssues[i].fields.assignee.key){
                console.log("dibandingin = " + backlogIssues[i+1].fields.assignee.key + " === " + backlogIssues[i].fields.assignee.key);
                queque = queque + (backlogIssues[i].fields.customfield_10106 + backlogIssues[i+1].fields.customfield_10106);
                pointQueque[i] = queque;
            }
        }else{
            console.log("dipanggil nih yo 2");
            queque = queque + backlogIssues[i].fields.customfield_10106;
            pointQueque[i] = {member : backlogIssues[i].fields.assignee, pointQueque : queque}
        }
        queque=0;
    }*/
    return tes;
}

exports.getPointMember = async()=>{

}
/*module.exports = function(db, tableSchema){
    return{
        tes : async()=>{

        }
    }
}*/
/*
module.exports = function (db, tableSchema) {

    return {

        getTotalIssueProgress : async(listProject)=>{
            var statistics = {};
            var totalIssueProgress = 0;
            for (let j = 0; j < listProject.length; j++) {
                Utils.requestData('/rest/api/2/search', 'GET', {jql:"project="+listProject[j].key+" and status='In Progress'",fields:'id, key, status'}, function (issues) {
                    //console.log("ISSUES = " + JSON.stringify(issues));
                    //console.log("Project key = " + listProject[j].key);
                    totalIssueProgress = totalIssueProgress + issues.total;
                    //console.log("Total Issue = " + totalIssueProgress);
                    if (j == (listProject.length - 1)) {
                        statistics.totalIssueProgress = totalIssueProgress;
                        return statistics;
                    }
                });
            }
        }
    }
}*/
