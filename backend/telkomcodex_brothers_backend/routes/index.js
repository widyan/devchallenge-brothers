/*var express = require('express');
var router = express.Router();*/

/* GET home page. */
/*router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;*/

module.exports = function (app, cache) {
    app.get('/listproject',cache.projectController().getListProject);
    app.get('/detailproject',cache.projectController().getDetailProject);
    app.get('/statistics',cache.projectController().getStatisticAllJiraProject);
    app.get('/bestproduct',cache.projectController().getBestProductPerformace);
    app.get('/bestteam',cache.projectController().getBestTeamPerformance);
    app.get('/pointmember',cache.projectController().getDetailPointMemberInProject);

    //login
    app.post('/login',cache.userController().login);
    //create event
    //
}
