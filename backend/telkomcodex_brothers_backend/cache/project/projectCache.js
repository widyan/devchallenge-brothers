var Utils = require('../../utils/utils');
var async = require('async');
const redis = require('redis');
const client = redis.createClient();

module.exports = function (controller) {
    return{
        getCacheStatistik : async(req, res)=>{
            client.get('detail',(err, result)=>{
                if(result){
                    res.status(200).send(result);
                }else{
                    controller.projectController().getStatisticAllJiraProject(req, res);
                }
            });
        }
    }
}